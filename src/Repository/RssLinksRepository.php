<?php

namespace App\Repository;

use App\Entity\RssLinks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RssLinks|null find($id, $lockMode = null, $lockVersion = null)
 * @method RssLinks|null findOneBy(array $criteria, array $orderBy = null)
 * @method RssLinks[]    findAll()
 * @method RssLinks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RssLinksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RssLinks::class);
    }

    // /**
    //  * @return RssLinks[] Returns an array of RssLinks objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RssLinks
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
