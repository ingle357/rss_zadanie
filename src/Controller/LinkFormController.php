<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\LinkType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\RssLinks;
use Psr\Log\LoggerInterface;

class LinkFormController extends AbstractController
{

    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/link/form", name="link_form")
     */
    public function index()
    {
        $this->logger->info(date("Y-m-d H:i:s")." Starting script.");

        $link = [];
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(LinkType::class, $link, [
            'action' => $this->generateUrl('link_form')
        ]);

        $request = Request::createFromGlobals();
        $form->handleRequest($request);
        $l = $request->get('link');


        if($form->isSubmitted() && $form->isValid()) {

            if (strpos(get_headers($l["link"])[0],"200 OK") !== false) {

                $rss = new \DOMDocument();
                $rss->load($l["link"]);
                $simplerss = simplexml_load_file($l["link"]);

                //rmf alike
                if ($rss->getElementsByTagName('rss')->length > 0) {

                    $i = 1;
                    foreach ($simplerss->channel->item as $item) {

                        $qb = $entityManager->createQueryBuilder();
                        $qb->select('count(r.title)');
                        $qb->from('App\Entity\RssLinks', 'r');
                        $qb->where('r.title = :title');
                        $qb->setParameter('title', $item->title);

                        if ($count = $qb->getQuery()->getSingleScalarResult() + 0 > 0) {
                            break;
                        }

                        $rssEntity = new RssLinks();
                        $rssEntity->setTitle($item->title);
                        $rssEntity->setPublished(date("Y-m-d H:i:s", strtotime($item->pubDate)));
                        $rssEntity->setContent($item->description);
                        $rssEntity->setAddDate(date("Y-m-d H:i:s"));
                        $entityManager->persist($rssEntity);
                        $entityManager->flush();

                        $this->logger->info(date("Y-m-d H:i:s")." Single article id:{$rssEntity->getId()}.");

                        $i++;
                        if ($i > 5) break;


                    }

                } elseif ($rss->getElementsByTagName('feed')->length > 0) {
                    //komputerswiat alike
                    if ($rss->getElementsByTagNameNS('http://search.yahoo.com/mrss/', '*')->length > 0) {

                        $i = 1;
                        foreach ($simplerss->entry as $item) {

                            $qb = $entityManager->createQueryBuilder();
                            $qb->select('count(r.title)');
                            $qb->from('App\Entity\RssLinks', 'r');
                            $qb->where('r.title = :title');
                            $qb->setParameter('title', $item->title);

                            if ($count = $qb->getQuery()->getSingleScalarResult() + 0 > 0) {
                                break;
                            }

                            $rssEntity = new RssLinks();
                            $rssEntity->setTitle($item->title);
                            $rssEntity->setPublished(date("Y-m-d H:i:s", strtotime($item->published)));
                            $rssEntity->setContent($item->summary);
                            $rssEntity->setAddDate(date("Y-m-d H:i:s"));
                            $entityManager->persist($rssEntity);
                            $entityManager->flush();

                            $this->logger->info(date("Y-m-d H:i:s")." Single article id:{$rssEntity->getId()}.");

                            $i++;
                            if ($i > 5) break;

                        }

                    }
                    //xmoon alike
                    else {

                        $i = 1;
                        foreach ($simplerss->entry as $item) {

                            $qb = $entityManager->createQueryBuilder();
                            $qb->select('count(r.title)');
                            $qb->from('App\Entity\RssLinks', 'r');
                            $qb->where('r.title = :title');
                            $qb->setParameter('title', $item->title);

                            if ($count = $qb->getQuery()->getSingleScalarResult() + 0 > 0) {
                                break;
                            }

                            $rssEntity = new RssLinks();
                            $rssEntity->setTitle($item->title);
                            $rssEntity->setPublished(date("Y-m-d H:i:s", strtotime($item->published)));
                            $rssEntity->setContent($item->content);
                            $rssEntity->setAddDate(date("Y-m-d H:i:s"));
                            $entityManager->persist($rssEntity);
                            $entityManager->flush();

                            $this->logger->info(date("Y-m-d H:i:s")." Single article id:{$rssEntity->getId()}.");

                            $i++;
                            if ($i > 5) break;

                        }

                    }
                }
            }
            else{
                return $this->render('base.html.twig', [
                    'controller_name' => 'LinkFormController',
                    'post_form' => $form->createView()
                ]);

            }
        }

        $this->logger->info(date("Y-m-d H:i:s")." Ending script.");

        return $this->render('link_form/index.html.twig', [
            'controller_name' => 'LinkFormController',
            'post_form' => $form->createView()
        ]);

    }

}
